#!/bin/bash

./folder --latex examples/trapezoid_0_0_0.txt > latex/trapezoid_0_0_0.tex
./folder --latex examples/trapezoid_0_0_1.txt > latex/trapezoid_0_0_1.tex
./folder --latex examples/trapezoid_0_0_-1.txt > latex/trapezoid_0_0_-1.tex
./folder --latex examples/trapezoid_0_1_0.txt > latex/trapezoid_0_1_0.tex
./folder --latex examples/trapezoid_0_1_1.txt > latex/trapezoid_0_1_1.tex
./folder --latex examples/trapezoid_0_1_-1.txt > latex/trapezoid_0_1_-1.tex
./folder --latex examples/trapezoid_0_-1_0.txt > latex/trapezoid_0_-1_0.tex
./folder --latex examples/trapezoid_0_-1_1.txt > latex/trapezoid_0_-1_1.tex
./folder --latex examples/trapezoid_0_-1_-1.txt > latex/trapezoid_0_-1_-1.tex
./folder --latex examples/trapezoid_1_0_0.txt > latex/trapezoid_1_0_0.tex
./folder --latex examples/trapezoid_1_0_1.txt > latex/trapezoid_1_0_1.tex
./folder --latex examples/trapezoid_1_0_-1.txt > latex/trapezoid_1_0_-1.tex
./folder --latex examples/trapezoid_1_1_0.txt > latex/trapezoid_1_1_0.tex
./folder --latex examples/trapezoid_1_1_1.txt > latex/trapezoid_1_1_1.tex
./folder --latex examples/trapezoid_1_1_-1.txt > latex/trapezoid_1_1_-1.tex
./folder --latex examples/trapezoid_1_-1_0.txt > latex/trapezoid_1_-1_0.tex
./folder --latex examples/trapezoid_1_-1_1.txt > latex/trapezoid_1_-1_1.tex
./folder --latex examples/trapezoid_1_-1_-1.txt > latex/trapezoid_1_-1_-1.tex
