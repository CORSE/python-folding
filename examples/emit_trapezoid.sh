#!/bin/bash

./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  0 -d4  0 -d6  0 > trapezoid_0_0_0.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  0 -d4  0 -d6  1 > trapezoid_0_0_1.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  0 -d4  0 -d6 -1 > trapezoid_0_0_-1.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  0 -d4  1 -d6  0 > trapezoid_0_1_0.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  0 -d4  1 -d6  1 > trapezoid_0_1_1.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  0 -d4  1 -d6 -1 > trapezoid_0_1_-1.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  0 -d4 -1 -d6  0 > trapezoid_0_-1_0.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  0 -d4 -1 -d6  1 > trapezoid_0_-1_1.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  0 -d4 -1 -d6 -1 > trapezoid_0_-1_-1.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  1 -d4  0 -d6  0 > trapezoid_1_0_0.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  1 -d4  0 -d6  1 > trapezoid_1_0_1.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  1 -d4  0 -d6 -1 > trapezoid_1_0_-1.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  1 -d4  1 -d6  0 > trapezoid_1_1_0.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  1 -d4  1 -d6  1 > trapezoid_1_1_1.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  1 -d4  1 -d6 -1 > trapezoid_1_1_-1.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  1 -d4 -1 -d6  0 > trapezoid_1_-1_0.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  1 -d4 -1 -d6  1 > trapezoid_1_-1_1.txt
./emit_foldable.py -i-cnt 5 -j-cnt 5 -k-cnt 5 -d2  1 -d4 -1 -d6 -1 > trapezoid_1_-1_-1.txt
