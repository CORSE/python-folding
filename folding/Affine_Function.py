## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

__all__ = [
  'Affine_Function',
  'Affine_Coefficient_Type',
]

import enum
from folding.Dimension import Dimension
from folding.Point     import Point


class Affine_Coefficient_Type(enum.Enum):
    # coefficient has not been defined yet, but we have already seen the first value.
    # can become DEFINED or TOP.
    UNCHANGED  = 0
    # coefficient has been defined, it can never change to another number, only to TOP
    DETERMINED = 1
    # coefficient had multiple possible values, it needed widening
    WIDENED    = 2


UNCHANGED, DETERMINED, WIDENED = Affine_Coefficient_Type


class Affine_Function:
    """
        An affine function from N scalar inputs to 1 scalar output.
        An affine function is made of two lists:
         - coeffs containing the coefficients of the function (may be UNCHANGED or 'Top')
         - indexes containing the value of the point we used to compute the coeffs
        For both lists, position 1 represents the inner most dimension, and position
        num_dimensions represents the outer most dimension.
        coeffs[0] contains the constant coefficient of the affine function
        indexes[0] is never used, and contains None (just there to avoid for uniform accesses in both list)
        indexes is only used internally to compute the coefficients of affine functions
    """

    def __init__(self, point: Point, value: int, widen_affine_functions: bool):
        self.num_dimensions = point.num_dimensions
        self.coeffs      = [value]
        for c in point.IV:
            self.coeffs.append(c)
        self.coeff_types = [UNCHANGED] * (self.num_dimensions + 1)

        self.widen_affine_functions = widen_affine_functions

    def canBeExtendedWith(self, other, q: Dimension, ) -> bool:
        # Both affine functions must have same number of inputs
        assert self.num_dimensions == other.num_dimensions

        # We  never  extend in  dimension  q  if any  coefficient  for
        # dimensions above (more outer) has already been computed
        for d in range(q + 1, self.num_dimensions + 1):
            assert self.coeff_types[d] == UNCHANGED

        # Because we always have a polyhedron of dimension d trying to
        # absorb a polyhedron of dimension d-1, the affine function of
        # other must be unchanged for all dimensions equale and above q
        for d in range(q, other.num_dimensions + 1):
            assert other.coeff_types[d] == UNCHANGED

        # Check compatibility by looping on all dimensions from 1 to q
        self_delta  = 0  # Contains the negative sum of the values contributed by other to self
        other_delta = 0  # Contains the negative sum of the values contributed by self to other

        for e in range(1, q + 1):
            self_coeff_type  = self.coeff_types[e]
            other_coeff_type = other.coeff_types[e]

            # Both coefficients already determined, must be the same
            if self_coeff_type == DETERMINED and other_coeff_type == DETERMINED:
                if self.coefficient(e) != other.coefficient(e):
                    return self.widen_affine_functions

            # Self's coeff determined but not other's one
            # Add to other's delta the result of the evalution of it's unchanged
            # coefficient with self's coefficient
            elif self_coeff_type == DETERMINED and other_coeff_type == UNCHANGED:
                other_delta = other_delta - other.initial_value(e) * self.coefficient(e)

            # Self's coeff not determined but other's one is
            # Add to self's delta the result of the evalution of it's unchanged
            # coefficient with other's coefficient
            elif self_coeff_type == UNCHANGED and other_coeff_type == DETERMINED:
                self_delta = self_delta - self.initial_value(e) * other.coefficient(e)

            # Both coefficients are not determined, evaluate them leads to zero
            elif self_coeff_type == UNCHANGED and other_coeff_type == UNCHANGED:
                self_delta  = self_delta  - 0
                other_delta = other_delta - 0

            # One of the coefficient is WIDENED, evaluate them leads to zero
            elif self_coeff_type == WIDENED or other_coeff_type == WIDENED:
                self_delta  = self_delta  - 0
                other_delta = other_delta - 0

        # If I am determined in dimension  q, the one we are extending
        # on,  then  we  must  check that  evaluating  other  with  my
        # determined coefficients and me with other determined coefficients
        # leads to the same value
        if self.coeff_types[q] == DETERMINED:  # Other is UNCHANGED in d
            if self.constant + self_delta != other.constant + other_delta:
                return self.widen_affine_functions  # Incompatible
            else:
                return True  # Compatible

        # Else I was not determined (nor other), then we can always find
        # a coefficient working
        elif self.coeff_types[q] == UNCHANGED:  # Other is UNCHANGED in d
            return True

        # Else I am widened and I can be extended by anyone
        else:  # Self is WIDENED
            return True

    def extendWith(self, other, q: Dimension):
        self_delta  = 0  # Contains the negative sum of the values contributed by other to self
        other_delta = 0  # Contains the negative sum of the values contributed by self to other

        for e in range(1, q + 1):
            self_coeff_type  = self.coeff_types[e]
            other_coeff_type = other.coeff_types[e]

            # Coefficients already determined
            if self_coeff_type == DETERMINED and other_coeff_type == DETERMINED:
                if self.coefficient(e) != other.coefficient(e):
                    self.widen_coefficient(e)

            # Self coeff affected but not other's one
            elif self_coeff_type == DETERMINED and other_coeff_type == UNCHANGED:
                other_delta = other_delta - other.initial_value(e) * self.coefficient(e)

            # Self coeff not affected but other's one affected
            elif self_coeff_type == UNCHANGED and other_coeff_type == DETERMINED:
                self_delta = self_delta - self.initial_value(e) * other.coefficient(e)
                self.set_coefficient(other.coefficient(e), e)

            # Both coefficients are not determined, evaluate them leads to zero
            # nothing to do
            elif self_coeff_type == UNCHANGED and other_coeff_type == UNCHANGED:
                pass

            # One of the coefficient is WIDENED, If I am not I must be widened
            # The check is kept like that to be consistent with to be extended
            elif self_coeff_type == WIDENED or other_coeff_type == WIDENED:
                # Other coefficient is WIDENED, I am not it wins on me !
                if self_coeff_type == DETERMINED and other_coeff_type == WIDENED:
                    self.widen_coefficient(e)

        # If I am determined in dimension  q, the one we are extending
        # on, then we must set to  WIDENED if evaluating other with my
        # coefficients leads to the incorrect value for other.
        if self.coeff_types[q] == DETERMINED:  # Other is UNCHANGED in d
            if self.constant + self_delta != other.constant + other_delta:
                 self.widen_coefficient(q)

        # Else I was not determined (nor other), then my coefficient
        # on dimension d is now known.
        elif self.coeff_types[q] == UNCHANGED:  # Other is UNCHANGED in d

            # Compute the remaining for other once we have we remove
            # all the contributions for coefficients determined in
            # self and not in other
            updated_other_remaining = other.constant + other_delta

            # At this point, all coefficients below d have been taken
            # into account:
            #   - for the contributions of self to other, it's in
            #     updated_other_remaining computed above
            #   - for the contributions of other to self, it's already
            #     in self remaining because of the coefficient update
            #     in the third case of the loop over dimensions above
            # The new coefficient is then just the differences of the
            # remaining values of the two functions / the growing step
            # in dimension d between the two functions (always 1 because
            # of the geometric criterion)
            new_coeff = updated_other_remaining - self.constant
            self.set_coefficient(new_coeff, q)

        # Else I am WIDENED, nothing to do
        else:
            pass

    def set_coefficient(self, coeff_val: int, q: Dimension):
        """
            Change coefficient for dimension q in self from UNCHANGED to DETERMINED.

            This also affects the constant of linear function self!
        """
        assert self.coeff_types[q] == UNCHANGED
        delta = -coeff_val * self.coeffs[q]
        self.coeff_types[q] = DETERMINED
        self.coeffs[q]      = coeff_val
        self.constant       = self.constant + delta

    def widen_coefficient(self, q: Dimension):
        """
            Change coefficient for dimension q in self from DETERMINED to WIDENED.
        """

        assert self.widen_affine_functions, 'needed widening, but it is forbidden'
        self.coeffs[q]      = None
        self.coeff_types[q] = WIDENED

    def coefficient(self, q: Dimension) -> int:
        """
            Get coefficient q from this affine function.
            The coefficient must have already been determined.
        """

        assert q > 0, 'use self.constant'
        assert self.coeff_types[q] == DETERMINED
        return self.coeffs[q]

    def initial_value(self, q: Dimension) -> int:
        """
            Get coordinate q from initial IV used to create this affine function.
            The coefficient must *not* have been determined.
        """

        assert q > 0, 'use self.constant'
        assert self.coeff_types[q] == UNCHANGED
        return self.coeffs[q]

    @property
    def constant(self) -> int:
        """
            Get the constant value of this function.
        """
        return self.coeffs[0]

    @constant.setter
    def constant(self, value: int):
        self.coeffs[0] = value

    def __repr__(self):
        return repr([t.name for t in self.coeff_types]) + ' ' + repr(self.coeffs)
