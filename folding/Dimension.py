## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

__all__ = [
  'Dimension',
]


class Dimension(int):
  def __new__(clss, value: int):
    assert value >= 0, 'Dimension cannot be negative'

    return super().__new__(clss, value)
