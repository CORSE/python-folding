## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

import argparse
import sys
import math
from typing    import *
from typing.io import *
from folding.Vector import Vector
from folding.images import *


################################################################################
### LOW LEVEL PRINTING API

def plot_document(doc: Document, file: IO):
  assert type(doc) is Document, doc

  print(TEMPLATE_START_DOCUMENT, file=file)
  for i in doc.images:
    plot_image(i, doc.min_coords, doc.max_coords, file=file)
  print(TEMPLATE_END_DOCUMENT, file=file)


def plot_image(image: Image, min_coords: Vector, max_coords: Vector, file: IO):
    max_coords = tuple(max_coords[:image.dimension])

    max_coords = max_coords or Vector([0])
    addplot = get_addplot_command(len(max_coords))

    label = f'Step {image.count}:'

    if image.description:
      description = image.description.replace('--->', '$\\rightarrow$')
      description = image.description.replace('->', '$\\rightarrow$')
      label += ' ' + description

    labels = ['none', 'k', 'j', 'i', 'fake']

    for dim, axis_label in enumerate(labels):
      labels[dim] = '$' + axis_label + '$'
      if dim in image.changed_dimensions:
        labels[dim] = '\\textcolor{red}{' + axis_label + '}'

    def emit(*args):
      print(*args, file=file)

    xmin = ymin = zmin = 0
    xmax = ymax = zmax = 0

    xmin = math.floor(min_coords[0])
    if len(min_coords) > 1:
      ymin = math.floor(min_coords[1])
    if len(min_coords) > 2:
      zmin = math.floor(min_coords[2])

    xmax = math.ceil(max_coords[0])
    if len(max_coords) > 1:
      ymax = math.ceil(max_coords[1])
    if len(max_coords) > 2:
      zmax = math.ceil(max_coords[2])

    emit(f'\\begin{{frame}}')
    emit(f'  \\frametitle{{{label}}}')
    emit()
    emit(f'  Changed dimensions:', *[labels[i] for i in image.changed_dimensions])
    emit(f'  \\vspace*{{1em}}')
    emit()
    emit(f'  \\begin{{tikzpicture}}')
    emit(f'    \\begin{{axis}}[')
    emit(f'      domain=-1:50,')
    emit(f'      xlabel={labels[1]},')
    emit(f'      ylabel={labels[2]},')
    emit(f'      zlabel={labels[3]},')
    emit(f'      enlarge x limits=0.2,')
    emit(f'      enlarge y limits=0.2,')
    emit(f'      enlarge z limits=0.2,')
    emit(f'      grid,')
    # emit(f'      axis x line=bottom,')
    # emit(f'      axis y line=center,')
    # emit(f'      axis z line=none,')
    # emit(f'      axis lines=center,')
    emit(f'      xtick={{{xmin},{xmin+1},...,{xmax}}}, % to have only integer axes ticks,')
    emit(f'      ytick={{{ymin},{ymin+1},...,{ymax}}}, % to have only integer axes ticks,')
    emit(f'      ztick={{{zmin},{zmin+1},...,{zmax}}}, % to have only integer axes ticks,')
    emit(f'      no markers,')
    emit(f'      every axis plot/.append style={{thick}}')
    emit(f'    ]')
    emit('      %% Fake plot (does not produce anything visible) that forces')
    emit('      %% the canvas to have at least a certain size.')
    emit('      %% If you don\'t do this weird errors happen™.')
    # must pad with ones (if we pad with zeroes the canvas is forced to have 0 along the padded dimensions)
    # otherwise weird errors happen
    emit('      \\' + addplot + '[] coordinates {', tikz_coordinates(max_coords, pad_with=1), '};')
    emit()
    for poly in image.polys:
      plot_polyhedron(poly.colour, poly.extreme_points, poly.label, poly.affine_functions, file=file)
      if poly is not image.polys[-1]:
        emit()
    emit(f'    \\end{{axis}}')
    emit(f'  \\end{{tikzpicture}}')
    emit(f'\\end{{frame}}')
    emit()


def plot_polyhedron(colour: Colour, extreme_points: List[Vector], label = None, affine_functions = [], file=sys.stdout):
  colour     = colour.latex
  num_points = len(extreme_points)

  # Fabian, what, we can't plot 4D plots ??????
  # Sorry Manu, I'll implement that next weekend.
  assert num_points in [1, 2, 4, 8], f'polyhedron has unsupported number of points: {extreme_points}'

  addplot = get_addplot_command(len(extreme_points[0]))

  def emit(*args):
    print(*args, file=file)

  def plot_label(label, affine_functions):
    if label:
      center = sum(extreme_points[1:], extreme_points[0]) / len(extreme_points)

      if num_points > 4:
        # If we are in 3D we plot the label `atop` the poly.
        # Oherwise it often gets obscured by lines.
        center = list(center)
        center[-1] = max(p[-1] for p in extreme_points)

      emit(
        ' ' * 5,
        '\\node at ' + tikz_coordinates(center),
        '{\\textcolor{' + colour + '}{' + label + '}};'
      )

      for i, fn in enumerate(affine_functions):
        variables = [None, 'k', 'j', 'i']

        coeff_strs = [str(fn.coeffs[0])]
        for dim in range(1, fn.num_dimensions + 1):
          ct = fn.coeff_types[dim]
          if ct.value == 0:  # UNCHANGED
            coeff_str = ('$\\bot_{' + str(fn.coeffs[dim]) + '}')
          elif ct.value == 2:  # WIDENED
            coeff_str = ('$\\top ')
          elif ct.value == 1:  # DETERMINED
            coeff_str = '$' + str(fn.coeffs[dim])
          else:
            assert False, 'Should not be there, ct=' + str(ct)
          coeff_str = coeff_str + variables[dim] + '$'
          coeff_strs.append(coeff_str)

        label = '+'.join(reversed(coeff_strs))

        emit(
          ' ' * 5,
          '\\node[anchor=north,yshift=-' + str(i * 8 + 3) + 'pt] at ' + tikz_coordinates(center),
          '{\\textcolor{' + colour + '}{\\tiny' + label + '}};'
        )

  def plot_line(start, end, offset):
    # shift points by offset
    start = start.shift(*offset)
    end   = end.shift(*offset)
    # tikzify
    start = tikz_coordinates(start)
    end   = tikz_coordinates(end)

    emit(
      ' ' * 5,
      '\\' + addplot + '[draw=' + colour + ']',
      'coordinates {',
    )
    emit(' ' * 7, start, end)
    emit(' ' * 5, '};')

  def plot_rectangle(lower_left, lower_right, upper_left, upper_right):
    emit(
      ' ' * 5,
      '\\' + addplot + '[draw=' + colour + ']',
      'coordinates {',
    )
    emit(' ' * 7, lower_left.shift(-0.0, -0.0), ' % lower left')
    emit(' ' * 7, lower_right.shift(+0.0, -0.0), ' % lower right')
    emit(' ' * 7, upper_right.shift(+0.0, +0.0), '% upper right')
    emit(' ' * 7, upper_left.shift(-0.0, +0.0), '% upper left')
    emit(' ' * 7, lower_left.shift(-0.0, -0.0), '% lower left')
    emit(' ' * 5, '};')

  if num_points in [1, 2, 4]:
    while len(extreme_points) < 4:
      extreme_points = extreme_points + extreme_points

    plot_rectangle(*extreme_points)
    plot_label(label, affine_functions)

  if num_points == 8:
    # bottom rectangle
    plot_rectangle(*extreme_points[0:4])
    # top rectangle
    plot_rectangle(*extreme_points[4:8])
    # connect the two rectangles
    plot_line(extreme_points[0], extreme_points[4], offset=[-0.0, -0.0])
    plot_line(extreme_points[1], extreme_points[5], offset=[+0.0, -0.0])
    plot_line(extreme_points[2], extreme_points[6], offset=[-0.0, +0.0])
    plot_line(extreme_points[3], extreme_points[7], offset=[+0.0, +0.0])
    plot_label(label, affine_functions)


def get_addplot_command(dimension):
  return {1: 'addplot', 2: 'addplot', 3: 'addplot3'}[dimension]


def tikz_coordinates(coords: Sequence[int], *, pad_with: int = 0) -> str:
  """
    Format a sequence of coordinates into a form tikz can use as coordinates.
  """

  assert all(isinstance(c, (int, float)) for c in coords), coords

  while len(coords) < 2:
    coords = list(coords)

    coords.append(pad_with)

  assert len(coords) <= 3, 'diagrams with >3 dimensions not supported yet!!'

  return '(' + ','.join(map(str, coords)) + ')'


TEMPLATE_START_DOCUMENT = r'''
\documentclass[xcolor={dvipsnames,usenames}]{beamer}

% Tikz
\usepackage{tikz}
\usetikzlibrary{fit}
\usetikzlibrary{calc}
\usetikzlibrary{backgrounds}

% Pgfplots
\usepackage{pgfplots}
\pgfplotsset{compat=1.13}

\tikzset{instance/.style={
    fill=red!50,
    circle, minimum size =6pt
  }
}
\tikzset{order/.style={
    thin,
    black!40,
    -latex
  }
}
\tikzset{polyhedron/.style={
    fill=red!20,
    draw=black
  }
}
\tikzset{instance invisible/.style={
   fill=none
  }
}

\begin{document}
'''

TEMPLATE_END_DOCUMENT = '''\
\end{document}
'''


################################################################################

if __name__ == '__main__':
  FNs = [
    old_eats_new,
    new_eats_old,
  ]

  FN_MAP = {
    fn.__name__.replace('_', '-'): fn for fn in FNs
  }

  p = argparse.ArgumentParser()
  p.add_argument(
    'ALGO',
    default='stdin',
    choices=['stdin'] + sorted(FN_MAP)
  )
  args = p.parse_args()

  if args.ALGO == 'stdin':
    doc = eval(sys.stdin.read())
  else:
    doc = FN_MAP[args.ALGO]()

  plot_document(doc, sys.stdout)
